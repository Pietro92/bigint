#include "BigInt.h"

namespace bigint
{

	#pragma region CONSTRUCTOR
	BigInt::BigInt() : _sign(false), _number("0") {}

	BigInt::BigInt(int value)
	{
		if (value == 0)
		{
			_sign = false;
			_number = "0";
			return;
		}

		_sign = value >= 0 ? false : true;
		value = !_sign ? value : value * -1;

		while (value > 0) {
			_number.push_back((value % 10) + '0');
			value /= 10;
		}

		reverse(_number.begin(), _number.end());
	}

	BigInt::BigInt(long long value)
	{
		if (value == 0)
		{
			_sign = false;
			_number = "0";
			return;
		}

		_sign = value >= 0 ? false : true;
		value = !_sign ? value : value * -1;

		while (value > 0) {
			_number.push_back((value % 10) + '0');
			value /= 10;
		}

		reverse(_number.begin(), _number.end());
	}

	BigInt::BigInt(const char* s)
	{
		if (s != nullptr)
		{
			_sign = s[0] == '-' ? true : false;
			if (!bigint::is_valid_number(s))	throw std::invalid_argument("Passing only number!");

			int endValue = _sign ? 1 : 0;
			for (int i = strlen(s) - 1; i >= endValue; i--)
			{
				_number.push_back(s[i]);
			}

			reverse(_number.begin(), _number.end());
		}
		else throw std::invalid_argument("nullptr is invalid");
	}

	BigInt::BigInt(const std::string& s)
	{
		_sign = s[0] == '-' ? true : false;
		if (!bigint::is_valid_number(s))	throw std::invalid_argument("Passing only number!");

		int endValue = _sign ? 1 : 0;
		for (int i = s.size() - 1; i >= endValue; i--)
		{
			_number.push_back(s[i]);
		}

		reverse(_number.begin(), _number.end());
	}
	#pragma endregion CONSTRUCTOR

	#pragma region CONSTRUCTOR_COPY
	BigInt::BigInt(const BigInt& num) : _sign(num._sign), _number(num._number) {}
	#pragma endregion CONSTRUCTOR_COPY

	#pragma region OPERATOR_COPY
	BigInt& BigInt::operator=(const BigInt& num)
	{
		_sign = num._sign;
		_number = num._number;

		return *this;
	}

	BigInt& BigInt::operator=(const std::string& s)
	{
		if (!bigint::is_valid_number(s))	throw std::invalid_argument("Passing only number!");

		BigInt num(s);
		_sign = num._sign;
		_number = num._number;

		return *this;
	}

	BigInt& BigInt::operator=(const long long& value)
	{
		BigInt num(value);
		_sign = num._sign;
		_number = num._number;

		return *this;
	}
	#pragma endregion OPERATOR_COPY

	#pragma region INCREMENT_DECREMENT
	BigInt& BigInt::operator++()
	{
		*this += 1;
		return *this;
	}

	BigInt BigInt::operator++(int)
	{
		BigInt num = *this;
		*this += 1;
		return num;
	}

	BigInt& BigInt::operator--()
	{
		*this -= 1;
		return *this;
	}

	BigInt BigInt::operator--(int)
	{
		BigInt num = *this;
		*this -= 1;
		return num;
	}
	#pragma endregion INCREMENT_DECREMENT

	#pragma region OPERATION_ADD
	BigInt BigInt::operator+(const BigInt& num) const
	{
		if (!_sign && num._sign)
		{
			BigInt temp = num;
			temp._sign = false;
			return *this - temp;
		}
		else if (_sign && !num._sign)
		{
			BigInt temp = *this;
			temp._sign = false;
			return -(temp - num);
		}

		BigInt result;
		result._number = "";

		// Create larger and smaller numbers for binary operation
		std::string larger;
		std::string smaller;
		std::tie(larger, smaller) = bigint::get_larger_and_smaller(_number, num._number);

		// Initialize carry and sum 
		short carry = 0;
		short sum = 0;

		for (int i = larger.size() - 1; i >= 0; i--)
		{
			const short a = larger[i] == '0' ? 0 : (short)(larger[i] - '0');
			const short b = smaller[i] == '0' ? 0 : (short)(smaller[i] - '0');
			sum = a + b + carry;
			result._number.push_back((sum % 10) + '0');
			carry = sum / (short)10;
		}

		// Check last carry equals 1 
		if (carry)
		{
			result._number.push_back(carry + '0');
		}

		reverse(result._number.begin(), result._number.end());

		// Check for sign
		// If the first number has negative sign or result is equals to zero so the result has negative value
		if (_sign && result._number != "0")
		{
			result._sign = true;
		}

		return result;
	}

	BigInt BigInt::operator+(const long long& num) const
	{
		return *this + BigInt(num);
	}

	BigInt BigInt::operator+(const std::string& num) const
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		return *this + BigInt(num);
	}

	BigInt& BigInt::operator+=(const BigInt& num)
	{
		*this = *this + num;
		return *this;
	}

	BigInt& BigInt::operator+=(const long long& num)
	{
		*this = *this + BigInt(num);
		return *this;
	}

	BigInt& BigInt::operator+=(const std::string& num)
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		*this = *this + BigInt(num);
		return *this;
	}

	BigInt BigInt::operator+() const
	{
		return *this;
	}
	#pragma endregion OPERATION_ADD

	#pragma region OPERATION_SUBTRACT
	BigInt BigInt::operator-(const BigInt& num) const
	{
		if (!_sign && num._sign)
		{
			BigInt temp = num;
			temp._sign = false;
			return *this + temp;
		}
		else if (_sign && !num._sign)
		{
			BigInt temp = *this;
			temp._sign = false;
			return -(temp + num);
		}

		BigInt result;
		result._number = "";

		// Create larger and smaller numbers for binary operation
		std::string larger;
		std::string smaller;
		std::tie(larger, smaller) = bigint::get_larger_and_smaller(_number, num._number);

		// Check for sign
		// If the first number has negative sign or result is equals to zero so the result has negative value
		if (larger == _number && _sign)
		{
			result._sign = true;
		}
		else if (larger == num._number && !num._sign)
		{
			result._sign = true;
		}

		// Initialize def
		short def = 0;

		for (int i = larger.size() - 1; i >= 0; i--)
		{
			short a = larger[i] == '0' ? 0 : (short)(larger[i] - '0');
			const short b = smaller[i] == '0' ? 0 : (short)(smaller[i] - '0');

			if (a < b)
			{
				bool check = false;
				for (int j = i - 1; j >= 0 && !check; j--)
				{
					short c = larger[j] == '0' ? 0 : (short)(larger[j] - '0');

					if (c > 0)
					{
						a += 10;
						c -= 1;

						// Recalculate number
						larger[j]--;
						for (int k = j + 1; k < i; k++)
						{
							larger[k] = '9';
						}

						check = true;
					}
				}
			}

			def = a - b;
			result._number.push_back((def % 10) + '0');
		}

		reverse(result._number.begin(), result._number.end());

		// Check zeros
		if (result._number == "0")
		{
			result._sign = false;
		}
		else
		{
			bigint::remove_zeros(result._number);
		}

		return result;
	}

	BigInt BigInt::operator-(const long long& num) const
	{
		return *this - BigInt(num);
	}

	BigInt BigInt::operator-(const std::string& num) const
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		return *this - BigInt(num);
	}

	BigInt& BigInt::operator-=(const BigInt& num)
	{
		*this = *this - num;
		return *this;
	}

	BigInt& BigInt::operator-=(const long long& num)
	{
		*this = *this - BigInt(num);
		return *this;
	}

	BigInt& BigInt::operator-=(const std::string& num)
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		*this = *this - BigInt(num);
		return *this;
	}

	BigInt BigInt::operator-() const
	{
		BigInt temp;
		temp._number = _number;

		if (_number != "0")
		{
			if (!_sign)
			{
				temp._sign = true;
			}
			else
			{
				temp._sign = false;
			}
		}
		return temp;
	}
	#pragma endregion OPERATION_SUBTRACT

	#pragma region OPERATION_MULTIPLY
	BigInt BigInt::operator*(const BigInt& num) const
	{
		if (_number == "0" || num._number == "0")
		{
			return BigInt("0");
		}
		else if (_number == "1")
		{
			return _sign ? -num : num;
		}
		else if (num._number == "1")
		{
			return num._sign ? -(*this) : *this;
		}

		BigInt result;
		std::string larger;
		std::string smaller;
		std::tie(larger, smaller) = bigint::get_larger_and_smaller_without_zeros(_number, num._number);

		long int zeros = 0;

		for (long int i = smaller.length() - 1; i >= 0; i--)
		{
			BigInt sum;
			sum._number = "";

			// When calculate multiply, is important add X zeros for iteration 
			bigint::add_zeros(sum._number, zeros);

			const short a = smaller[i] == '0' ? 0 : (short)(smaller[i] - '0');
			short carry = 0;

			for (long int j = larger.length() - 1; j >= 0; j--)
			{
				const short b = larger[j] == '0' ? 0 : (short)(larger[j] - '0');
				short mult = (a * b) + carry;

				if (mult > 0)
				{
					sum._number.push_back((mult % 10) + '0');
					carry = mult / 10;
				}
				else
				{
					sum._number.push_back('0');
				}
			}

			// Check last carry
			if (carry)
			{
				sum._number.push_back(carry + '0');
			}

			reverse(sum._number.begin(), sum._number.end());
			result += sum;

			//Increment zeros
			zeros++;
		}

		// Sign control
		if (_sign ^ num._sign)
		{
			result._sign = true;
		}

		return result;
	}

	BigInt BigInt::operator*(const long long& num) const
	{
		return *this * BigInt(num);
	}

	BigInt BigInt::operator*(const std::string& num) const
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		return *this * BigInt(num);
	}

	BigInt& BigInt::operator*=(const BigInt& num)
	{
		*this = *this * num;
		return *this;
	}

	BigInt& BigInt::operator*=(const long long& num)
	{
		*this = *this * BigInt(num);
		return *this;
	}

	BigInt& BigInt::operator*=(const std::string& num)
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		*this = *this * BigInt(num);
		return *this;
	}
	#pragma endregion OPERATION_MULTIPLY

	#pragma region OPERATION_DIVIDE
	std::tuple<BigInt, BigInt> divide(const BigInt& dividend, const BigInt& divisor) {
		BigInt quotient, remainder, temp;

		temp = divisor;
		quotient = 1;
		while (temp < dividend)
		{
			quotient++;
			temp += divisor;
		}
		if (temp > dividend)
		{
			quotient--;
			remainder = dividend - (temp - divisor);
		}

		return std::make_tuple(quotient, remainder);
	}

	BigInt BigInt::operator/(const BigInt& num) const
	{
		if (_number == "0" || _number == "1")
		{
			return 0;
		}
		else if (num._number == "0")
		{
			throw std::invalid_argument("Is impossible to divide one number for zero!!!");
		}
		else if (num._number == "1")
		{
			return num._sign ? -(*this) : *this;
		}

		BigInt dividend = abs(*this);
		BigInt divisor = abs(num);
		BigInt result;

		if (dividend <= LLONG_MAX && divisor <= LLONG_MAX)
		{
			result = std::stoll(dividend._number) / std::stoll(divisor._number);
		}
		else if (dividend == divisor)
		{
			result = 1;
		}
		else if (bigint::is_power_of_ten(divisor._number))
		{
			// divisor._number.size() - 1 because is important count only zeros
			result._number = dividend._number.substr(0, dividend._number.size() - (divisor._number.size() - 1));
		}
		else
		{
			result._number = "";

			BigInt chunk;
			BigInt quotient;
			BigInt remainder;
			size_t chunk_index = 0;

			remainder._number = dividend._number.substr(chunk_index, divisor._number.size() - 1);
			chunk_index = divisor._number.size() - 1;
			while (chunk_index < dividend._number.size())
			{
				chunk._number = remainder._number.append(1, dividend._number[chunk_index]);
				chunk_index++;
				while (chunk < divisor) {
					result._number += "0";
					if (chunk_index < dividend._number.size()) {
						chunk._number.append(1, dividend._number[chunk_index]);
						chunk_index++;
					}
					else
						break;
				}
				if (chunk == divisor) {
					result._number += 1;
					remainder = 0;
				}
				else if (chunk > divisor)
				{
					bigint::remove_zeros(chunk._number);
					std::tie(quotient, remainder) = divide(chunk, divisor);
					result._number += quotient._number;
				}
			}
		}
		bigint::remove_zeros(result._number);

		// Sign control
		if (_sign == num._sign)
		{
			result._sign = false;
		}
		else
		{
			result._sign = true;
		}

		return result;
	}

	BigInt BigInt::operator/(const long long& num) const
	{
		return *this / BigInt(num);
	}

	BigInt BigInt::operator/(const std::string& num) const
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		return *this / BigInt(num);
	}

	BigInt& BigInt::operator/=(const BigInt& num)
	{
		*this = *this / num;
		return *this;
	}

	BigInt& BigInt::operator/=(const long long& num)
	{
		*this = *this / BigInt(num);
		return *this;
	}

	BigInt& BigInt::operator/=(const std::string& num)
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		*this = *this / BigInt(num);
		return *this;
	}
	#pragma endregion OPERATION_DIVIDE

	#pragma region OPERATION_MODULE
	BigInt BigInt::operator%(const BigInt& num) const
	{
		BigInt dividend = abs(*this);
		BigInt divisor = abs(num);

		if (num == 0)
		{
			throw std::invalid_argument("Is impossible to divide one number for zero!!!");
		}
		if (*this == 0 || divisor == 1 || dividend == divisor)
		{
			return BigInt("0");
		}

		BigInt result;
		if (dividend <= LLONG_MAX && divisor <= LLONG_MAX)
		{
			result = std::stoll(dividend._number) % std::stoll(divisor._number);
		}
		else if (dividend < divisor)
		{
			result = dividend;
		}
		else if (bigint::is_power_of_ten(divisor._number))
		{
			// divisor._number.size() - 1 because is important count only zeros
			result._number = dividend._number.substr(dividend._number.size() - (divisor._number.size() - 1));
		}
		else
		{
			result = dividend - (dividend / divisor) * divisor;
		}
		bigint::remove_zeros(result._number);

		result._sign = _sign;
		if (result == 0)
		{
			result._sign = false;
		}

		return result;
	}

	BigInt BigInt::operator%(const long long& num) const
	{
		return *this % BigInt(num);
	}

	BigInt BigInt::operator%(const std::string& num) const
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		return *this % BigInt(num);
	}

	BigInt& BigInt::operator%=(const BigInt& num)
	{
		*this = *this % num;
		return *this;
	}

	BigInt& BigInt::operator%=(const long long& num)
	{
		*this = *this % BigInt(num);
		return *this;
	}

	BigInt& BigInt::operator%=(const std::string& num)
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		*this = *this % BigInt(num);
		return *this;
	}
	#pragma endregion OPERATION_MODULE

	#pragma region CONDITION
	bool BigInt::operator<(const BigInt& num) const
	{
		if (_number == num._number)
		{
			return false;
		}
		if (_sign == num._sign)
		{
			std::string larger;
			std::string smaller;
			std::tie(larger, smaller) = bigint::get_larger_and_smaller_without_zeros(_number, num._number);

			return _number == smaller;
		}
		return _sign;
	}

	bool BigInt::operator<(const long long& num) const
	{
		return *this < BigInt(num);
	}

	bool BigInt::operator<(const std::string& num) const
	{
		return *this < BigInt(num);
	}

	bool BigInt::operator<=(const BigInt& num) const
	{
		return (*this < num) || (*this == num);
	}

	bool BigInt::operator<=(const long long& num) const
	{
		return !(*this > BigInt(num));
	}

	bool BigInt::operator<=(const std::string& num) const
	{
		return !(*this > BigInt(num));
	}

	bool BigInt::operator>(const BigInt& num) const
	{
		return !((*this < num) || (*this == num));
	}

	bool BigInt::operator>(const long long& num) const
	{
		return *this > BigInt(num);
	}

	bool BigInt::operator>(const std::string& num) const
	{
		return *this > BigInt(num);
	}

	bool BigInt::operator>=(const BigInt& num) const
	{
		return !(*this < num);
	}

	bool BigInt::operator>=(const long long& num) const
	{
		return !(*this < BigInt(num));
	}

	bool BigInt::operator>=(const std::string& num) const
	{
		return !(*this < BigInt(num));
	}

	bool BigInt::operator!=(const BigInt& num) const
	{
		return !(*this == num);
	}

	bool BigInt::operator!=(const long long& num) const
	{
		return !(*this == BigInt(num));
	}

	bool BigInt::operator!=(const std::string& num) const
	{
		return !(*this == BigInt(num));
	}

	bool BigInt::operator==(const BigInt& num) const
	{
		return (_sign == num._sign) and (_number == num._number);
	}

	bool BigInt::operator==(const long long& num) const
	{
		return *this == BigInt(num);
	}

	bool BigInt::operator==(const std::string& num) const
	{
		return *this == BigInt(num);
	}
	#pragma endregion CONDITION

	#pragma region BITWISE_AND
	BigInt BigInt::operator&(const BigInt& num) const
	{
		BigInt lnum = to_binary();
		BigInt rnum = num.to_binary();
		BigInt result;
		result._number = "";

		std::string larger;
		std::string smaller;
		std::tie(larger, smaller) = bigint::get_larger_and_smaller(lnum._number, rnum._number);

		for (long int i = 0; i < larger.size(); i++)
		{
			const short a = larger[i] == '0' ? 0 : (short)(larger[i] - '0');
			const short b = smaller[i] == '0' ? 0 : (short)(smaller[i] - '0');

			if (a == 1 && b == 1)
			{
				result._number.push_back(1 + '0');
			}
			else
			{
				result._number.push_back('0');
			}
		}
		remove_zeros(result._number);

		return result.from_binary_to_bigint();
	}

	BigInt BigInt::operator&(const std::string& num) const
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		return *this & BigInt(num);
	}

	BigInt BigInt::operator&(const long long& num) const
	{
		return *this & BigInt(num);
	}

	BigInt& BigInt::operator&=(const BigInt& num)
	{
		*this = *this & num;
		return *this;
	}

	BigInt& BigInt::operator&=(const std::string& num)
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		*this = *this & BigInt(num);
		return *this;
	}

	BigInt& BigInt::operator&=(const long long& num)
	{
		*this = *this & BigInt(num);
		return *this;
	}
	#pragma endregion BITWISE_AND

	#pragma region BITWISE_OR
	BigInt BigInt::operator|(const BigInt& num) const
	{
		BigInt lnum = to_binary();
		BigInt rnum = num.to_binary();
		BigInt result;
		result._number = "";

		std::string larger;
		std::string smaller;
		std::tie(larger, smaller) = bigint::get_larger_and_smaller(lnum._number, rnum._number);

		for (long int i = 0; i < larger.size(); i++)
		{
			const short a = larger[i] == '0' ? 0 : (short)(larger[i] - '0');
			const short b = smaller[i] == '0' ? 0 : (short)(smaller[i] - '0');

			if (a == 1 || b == 1)
			{
				result._number.push_back(1 + '0');
			}
			else
			{
				result._number.push_back('0');
			}
		}
		remove_zeros(result._number);

		return result.from_binary_to_bigint();
	}

	BigInt BigInt::operator|(const std::string& num) const
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		return *this | BigInt(num);
	}

	BigInt BigInt::operator|(const long long& num) const
	{
		return *this | BigInt(num);
	}

	BigInt& BigInt::operator|=(const BigInt& num)
	{
		*this = *this | num;
		return *this;
	}

	BigInt& BigInt::operator|=(const std::string& num)
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		*this = *this | BigInt(num);
		return *this;
	}

	BigInt& BigInt::operator|=(const long long& num)
	{
		*this = *this | BigInt(num);
		return *this;
	}
	#pragma endregion BITWISE_OR
	
	#pragma region BITWISE_XOR
	BigInt BigInt::operator^(const BigInt& num) const
	{
		BigInt lnum = to_binary();
		BigInt rnum = num.to_binary();
		BigInt result;
		result._number = "";

		std::string larger;
		std::string smaller;
		std::tie(larger, smaller) = bigint::get_larger_and_smaller(lnum._number, rnum._number);

		for (long int i = 0; i < larger.size(); i++)
		{
			const short a = larger[i] == '0' ? 0 : (short)(larger[i] - '0');
			const short b = smaller[i] == '0' ? 0 : (short)(smaller[i] - '0');

			if ((a + b) % 2)
			{
				result._number.push_back(1 + '0');
			}
			else
			{
				result._number.push_back('0');
			}
		}
		remove_zeros(result._number);

		return result.from_binary_to_bigint();
	}

	BigInt BigInt::operator^(const std::string& num) const
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		return *this ^ BigInt(num);
	}

	BigInt BigInt::operator^(const long long& num) const
	{
		return *this ^ BigInt(num);
	}

	BigInt& BigInt::operator^=(const BigInt& num)
	{
		*this = *this ^ num;
		return *this;
	}

	BigInt& BigInt::operator^=(const std::string& num)
	{
		if (!bigint::is_valid_number(num))	throw std::invalid_argument("Passing only number!");
		*this = *this ^ BigInt(num);
		return *this;
	}

	BigInt& BigInt::operator^=(const long long& num)
	{
		*this = *this ^ BigInt(num);
		return *this;
	}
	#pragma endregion BITWISE_XOR

	#pragma region SHIFT
	BigInt BigInt::operator<<(const int& num) const
	{
		BigInt mult = pow(2, num);
		return *this * mult;
	}

	BigInt BigInt::operator>>(const int& num) const
	{
		BigInt mult = pow(2, num);
		return *this / mult;
	}
	
	BigInt& BigInt::operator<<=(const int& num)
	{
		BigInt mult = pow(2, num);
		*this *= mult;
		return *this;
	}

	BigInt& BigInt::operator>>=(const int& num)
	{
		BigInt mult = pow(2, num);
		*this /= mult;
		return *this;
	}
	#pragma endregion SHIFT

	#pragma region IOSTREAM
	std::istream& operator>>(std::istream& in, BigInt& a)
	{
		std::string input;
		in >> input;
		a = BigInt(input);

		return in;
	}

	std::ostream& operator<<(std::ostream& out, const BigInt& a)
	{
		if (a._number.length() == 0 || a._number[0] == '0')
		{
			return out << "0";
		}

		if (a._sign)	out << "-";
		for (int i = 0; i < a._number.size(); i++)
			out << a._number[i];
		return out;
	}
	#pragma endregion IOSTREAM

	#pragma region CONVERSION
	std::string BigInt::to_string() const
	{
		return this->_sign == '-' ? "-" + this->_number : this->_number;
	}

	BigInt BigInt::to_binary() const
	{
		BigInt decimal = *this;
		BigInt binary;
		BigInt remainder;
		BigInt product(1);

		while (decimal != 0)
		{
			remainder = decimal % 2;
			binary += remainder * product;
			decimal /= 2;
			product *= 10;
		}

		return binary;
	}

	BigInt BigInt::from_binary_to_bigint() const
	{
		BigInt binary = *this;
		BigInt decimal;
		BigInt remainder;
		BigInt product(1);

		while (binary != 0)
		{
			remainder = binary % 10;
			decimal += remainder * product;
			binary /= 10;
			product *= 2;
		}
		return decimal;
	}
	#pragma endregion CONVERSION
}