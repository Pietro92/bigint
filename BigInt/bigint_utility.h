#pragma once
#include <iostream>
#include <string>
#include <tuple>

namespace bigint
{
	inline const bool check_first_is_larger(const std::string& num1, const std::string& num2)
	{
		for (long int i = 0; i < num1.length(); i++)
		{
			const short a = num1[i] == '0' ? 0 : (short)(num1[i] - '0');
			const short b = num2[i] == '0' ? 0 : (short)(num2[i] - '0');

			if (a > b)
			{
				return true;
			}
			else if (a < b)
			{
				return false;
			}
		}
		return false;
	}

	inline void add_zeros(std::string& number, size_t zeros)
	{
		number = std::string(zeros, '0') + number;
	}

	inline void remove_zeros(std::string& number)
	{
		int count = 0;
		bool check = false;
		for (int i = 0; i < number.size() && !check; i++)
		{
			if (number[i] == '0')
			{
				count++;
			}
			else
			{
				check = true;
			}
		}
		number = number.substr(count);
	}

	inline std::tuple<std::string, std::string> get_larger_and_smaller(const std::string& num1, const std::string& num2)
	{
		std::string larger = "";
		std::string smaller = "";

		if (num1.size() > num2.size() || (num1.size() == num2.size() && check_first_is_larger(num1, num2)))
		{
			larger = num1;
			smaller = num2;
		}
		else
		{
			larger = num2;
			smaller = num1;
		}

		// call the method for insert enough zero to fill smaller number
		add_zeros(smaller, larger.size() - smaller.size());

		return std::make_tuple(larger, smaller);
	}

	inline std::tuple<std::string, std::string> get_larger_and_smaller_without_zeros(const std::string& num1, const std::string& num2)
	{
		std::string larger = "";
		std::string smaller = "";

		if (num1.size() > num2.size() || (num1.size() == num2.size() && check_first_is_larger(num1, num2)))
		{
			larger = num1;
			smaller = num2;
		}
		else
		{
			larger = num2;
			smaller = num1;
		}

		return std::make_tuple(larger, smaller);
	}

	inline bool is_valid_number(const std::string& num)
	{
		bool success = true;
		int start = num[0] == '-' ? 1 : 0;
		for (int i = start; i < num.length() && success; i++)
		{
			if (!isdigit(num[i])) success = false;
		}
		return success;
	}

	inline bool is_valid_number(const char* num)
	{
		bool success = true;
		int start = num[0] == '-' ? 1 : 0;
		for (int i = start; i < strlen(num) && success; i++)
		{
			if (!isdigit(num[i])) success = false;
		}
		return success;
	}

	inline bool is_power_of_ten(const std::string& num)
	{
		if (num[0] != '1')
		{
			return false;
		}
		else
		{
			for (int i = 1; i < num.length(); i++)
			{
				if (num[i] != '0')
				{
					return false;
				}
			}
		}
		return true;
	}
}