#pragma once
#include <iostream>
#include <stdio.h>
#include <string>
#include <cmath>
#include <tuple>
#include <stdexcept>

#include "bigint_utility.h"

namespace bigint 
{
	class BigInt {

	private:
		bool _sign;
		std::string _number;

	public:
		BigInt();
		BigInt(const BigInt& num);
		BigInt(int value);
		BigInt(long long value);
		BigInt(const char* s);
		BigInt(const std::string& s);

		BigInt& operator=(const BigInt& num);
		BigInt& operator=(const long long& value);
		BigInt& operator=(const std::string& s);

		// Binary operator
		BigInt operator+(const BigInt& num) const;
		BigInt operator-(const BigInt& num) const;
		BigInt operator*(const BigInt& num) const;
		BigInt operator/(const BigInt& num) const;
		BigInt operator%(const BigInt& num) const;

		// Arithmetic operator
		BigInt& operator+=(const BigInt& num);
		BigInt& operator-=(const BigInt& num);
		BigInt& operator*=(const BigInt& num);
		BigInt& operator/=(const BigInt& num);
		BigInt& operator%=(const BigInt& num);

		// Unary operator
		BigInt operator+() const;
		BigInt operator-() const;

		// Pre-increment || Post-increment
		BigInt& operator++();
		BigInt operator++(int);
		BigInt& operator--();
		BigInt operator--(int);

		// Binary Sum
		BigInt operator+(const long long& num) const;
		BigInt operator+(const std::string& num) const;

		// Arithmetic Sum
		BigInt& operator+=(const long long& num);
		BigInt& operator+=(const std::string& num);

		// Binary Subtraction 
		BigInt operator-(const long long& num) const;
		BigInt operator-(const std::string& num) const;

		// Arithmetic Sub
		BigInt& operator-=(const long long& num);
		BigInt& operator-=(const std::string& num);

		// Binary Multiply 
		BigInt operator*(const long long& num) const;
		BigInt operator*(const std::string& num) const;

		// Arithmetic Multiply
		BigInt& operator*=(const long long& num);
		BigInt& operator*=(const std::string& num);

		// Binary Divide 
		BigInt operator/(const long long& num) const;
		BigInt operator/(const std::string& num) const;

		// Arithmetic Divide
		BigInt& operator/=(const long long& num);
		BigInt& operator/=(const std::string& num);

		// Binary Modulus 
		BigInt operator%(const long long& num) const;
		BigInt operator%(const std::string& num) const;

		// Arithmetic Modulus
		BigInt& operator%=(const long long& num);
		BigInt& operator%=(const std::string& num);

		// Relational operations
		bool operator<(const BigInt& num) const;
		bool operator<(const long long& num) const;
		bool operator<(const std::string& num) const;
		bool operator<=(const BigInt& num) const;
		bool operator<=(const long long& num) const;
		bool operator<=(const std::string& num) const;

		bool operator>(const BigInt& num) const;
		bool operator>(const long long& num) const;
		bool operator>(const std::string& num) const;
		bool operator>=(const BigInt& num) const;
		bool operator>=(const long long& num) const;
		bool operator>=(const std::string& num) const;

		bool operator!=(const BigInt& num) const;
		bool operator!=(const long long& num) const;
		bool operator!=(const std::string& num) const;

		bool operator==(const BigInt& num) const;
		bool operator==(const long long& num) const;
		bool operator==(const std::string& num) const;

		// Bitwise AND Operation
		BigInt operator&(const BigInt& num) const;
		BigInt operator&(const std::string& num) const;
		BigInt operator&(const long long& num) const;
		BigInt& operator&=(const BigInt& num);
		BigInt& operator&=(const std::string& num);
		BigInt& operator&=(const long long& num);

		// Bitwise OR Operation
		BigInt operator|(const BigInt& num) const;
		BigInt operator|(const std::string& num) const;
		BigInt operator|(const long long& num) const;
		BigInt& operator|=(const BigInt& num);
		BigInt& operator|=(const std::string& num);
		BigInt& operator|=(const long long& num);

		// Bitwise XOR Operation
		BigInt operator^(const BigInt& num) const;
		BigInt operator^(const std::string& num) const;
		BigInt operator^(const long long& num) const;
		BigInt& operator^=(const BigInt& num);
		BigInt& operator^=(const std::string& num);
		BigInt& operator^=(const long long& num);

		// Shift Operation
		BigInt operator<<(const int& num) const;
		BigInt operator>>(const int& num) const;
		BigInt& operator<<=(const int& num);
		BigInt& operator>>=(const int& num);

		// IOStream
		friend std::istream& operator>>(std::istream&, BigInt&);
		friend std::ostream& operator<<(std::ostream&, const BigInt&);

		// Conversation
		std::string to_string() const;
		BigInt to_binary() const;
		BigInt from_binary_to_bigint() const;
	};

	inline BigInt abs(const BigInt& num)
	{
		return num < 0 ? -num : num;
	}

	inline BigInt pow10(size_t exp) 
	{
		return BigInt("1" + std::string(exp, '0'));
	}

	inline BigInt pow(const BigInt& base, int exp)
	{
		if (exp < 0) {
			if (base == 0)	throw std::invalid_argument("Cannot divide to zero!");
			return abs(base) == 1 ? base : 0;
		}
		if (exp == 0) {
			if (base == 0)	throw std::invalid_argument("Zero cannot be raised to zero");
			return 1;
		}

		BigInt result = base;
		BigInt result_odd = 1;

		while (exp > 1) {
			if (exp % 2)
				result_odd *= result;
			result *= result;
			exp /= 2;
		}
		return result * result_odd;
	}

	inline BigInt pow(const long long& base, int exp)
	{
		return pow(BigInt(base), exp);
	}

	inline BigInt pow(const std::string& base, int exp)
	{
		return pow(BigInt(base), exp);
	}

	inline BigInt pow(const BigInt& base, BigInt exp)
	{
		if (exp < 0) {
			if (base == 0)	throw std::invalid_argument("Cannot divide to zero!");
			return abs(base) == 1 ? base : 0;
		}
		if (exp == 0) {
			if (base == 0)	throw std::invalid_argument("Zero cannot be raised to zero");
			return 1;
		}

		BigInt result = base;
		BigInt result_odd = 1;

		while (exp > 1) {
			if (exp % 2 != 0)
				result_odd *= result;
			result *= result;
			exp /= 2;
		}
		return result * result_odd;
	}

	inline BigInt sqrt(const BigInt& num)
	{
		if (num < 0)
			throw std::invalid_argument("Invalid number!");

		if (num == 0)
			return 0;
		else if (num < 4)
			return 1;
		else if (num < 9)
			return 2;
		else if (num < 16)
			return 3;

		BigInt prev = -1;
		BigInt current = pow10(num.to_string().size() / 2 - 1);

		while (abs(current - prev) > 1) 
		{
			prev = current;
			current = (num / prev + prev) / 2;
		}

		return current;
	}
}