#include <iostream>
#include "BigInt.h"

void construction_debug()
{
	bigint::BigInt x("348734834853836786386736");
	bigint::BigInt y(535);

	std::cout << "X:" << x << " Y:" << y << std::endl;
}

void io_debug()
{
	bigint::BigInt x;

	std::cout << "Insert value:";
	std::cin >> x;
	std::cout << x << std::endl;
}

void sum_debug()
{
	bigint::BigInt x(-100);
	bigint::BigInt y(535);
	bigint::BigInt z = x + y;

	std::cout << "X:" << x << " Y:" << y << " Z:" << z << std::endl;

	bigint::BigInt i("0");

	std::cout << i << std::endl;
	i++;
	std::cout << i << std::endl;
	i += 20;
	std::cout << i << std::endl;

	bigint::BigInt j("-4");
	j++;
	std::cout << j << std::endl;
	j += i;
	std::cout << j << std::endl;
}

void subtraction_debug()
{
	bigint::BigInt x("10000000");
	bigint::BigInt y("1000000");
	std::cout << x - y << std::endl;

	bigint::BigInt a("-10");
	bigint::BigInt b(10);
	std::cout << a - b << std::endl;

	bigint::BigInt c("100");
	bigint::BigInt d(-10);
	std::cout << c - d << std::endl;

	bigint::BigInt i("0");

	std::cout << i << std::endl;
	i--;
	std::cout << i << std::endl;
	i -= 20;
	std::cout << i << std::endl;

	bigint::BigInt j("-4");
	j--;
	std::cout << j << std::endl;
	j -= i;
	std::cout << j << std::endl;
}

void multiply_debug()
{
	bigint::BigInt x(10);
	bigint::BigInt y("555");
	std::cout << x * y << std::endl;

	bigint::BigInt a(-234);
	bigint::BigInt b(-1111);
	std::cout << a * b << std::endl;

	bigint::BigInt c("0");
	bigint::BigInt d(-11);
	std::cout << c * d << std::endl;

	bigint::BigInt i(-234);
	i *= -1;
	std::cout << i << std::endl;
	i *= bigint::BigInt("-2");
	std::cout << i << std::endl;
	i *= bigint::BigInt("-20120");
	std::cout << i << std::endl;
}

void divide_debug()
{
	bigint::BigInt y(1512112111247874);
	bigint::BigInt x("254452455245521452247852");
	std::cout << x / y << std::endl;

	bigint::BigInt a(-234);
	bigint::BigInt b(455565551122555);
	std::cout << a / b << std::endl;

	bigint::BigInt c("112552542");
	bigint::BigInt d(12);
	std::cout << c / d << std::endl;

	bigint::BigInt i(5544565);
	i /= -1;
	std::cout << i << std::endl;
	i /= bigint::BigInt("-2");
	std::cout << i << std::endl;
	i /= bigint::BigInt("121");
	std::cout << i << std::endl;
}

void modulus_debug()
{
	bigint::BigInt x("35648888954457811");
	bigint::BigInt y(1115244);
	std::cout << x % y << std::endl;

	bigint::BigInt a(-234);
	bigint::BigInt b(4);
	std::cout << a % b << std::endl;

	bigint::BigInt c("112552542");
	bigint::BigInt d(12);
	std::cout << c % d << std::endl;

	bigint::BigInt i(5544565);
	i %= -1;
	std::cout << i << std::endl;
	i %= bigint::BigInt("-2");
	std::cout << i << std::endl;
	i %= bigint::BigInt("121");
	std::cout << i << std::endl;
}

void mathematics_debug()
{
	bigint::BigInt x("344444344443458584475626516145");
	bigint::BigInt y("4");
	bigint::BigInt z(569);

	std::cout << bigint::pow(x, 3) << std::endl; 
	std::cout << bigint::pow(z, 10) << std::endl;
	std::cout << bigint::pow(y, y) << std::endl;
	std::cout << bigint::pow10(10) << std::endl;
	std::cout << bigint::sqrt(y) << std::endl;
}

void bitwise_debug()
{
	bigint::BigInt x("151445585821818");
	bigint::BigInt y("24158585");
	std::cout << (x & 24158585) << std::endl;
	x &= 24158585;
	std::cout << x << std::endl;

	bigint::BigInt z = 187;
	z |= 112;
	std::cout << z << std::endl;
	std::cout << (bigint::BigInt("138874478") & 12838) << std::endl;
	
	bigint::BigInt a = "128387474";
	a ^= 1028383838;
	std::cout << a << std::endl;
}

void shift_debug()
{
	bigint::BigInt x = 102039;
	std::cout << (x << 12) << std::endl;

	bigint::BigInt a = 455851;
	a >>= 100;
	std::cout << a << std::endl;
}

int main()
{
	construction_debug();
	io_debug();
	sum_debug();
	subtraction_debug();
	multiply_debug();
	divide_debug();
	modulus_debug();
	mathematics_debug();
	bitwise_debug();
	shift_debug();

	return 0;
}